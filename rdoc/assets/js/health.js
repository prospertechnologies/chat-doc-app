
require('../css/health.scss');

$(document).ready(function() {

    $('#stressSlider')[0].oninput = function () {
        //output.innerHTML = this.value;

        // $('#stressSlider').css('backgroundColor', 'red');
        var stressSlider = $('#stressSlider');
        if (stressSlider.val() == '-1') {
            console.log("make green");
            stressSlider.removeClass('high');
            stressSlider.removeClass('middle');
            stressSlider.addClass('low');
        } else if (stressSlider.val() == '0') {
            console.log("make orange");
            stressSlider.addClass('middle');
            stressSlider.removeClass('high');
            stressSlider.removeClass('low');
        } else if (stressSlider.val() == '1') {
            console.log("make red");
            stressSlider.addClass('high');
            stressSlider.removeClass('low');
            stressSlider.removeClass('middle');
        }
    }
});