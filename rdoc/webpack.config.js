var Encore = require('@symfony/webpack-encore');

Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    .addEntry('ios', './assets/js/ios.js')
    .addEntry('footer', './assets/js/footer.js')
    .addEntry('me', './assets/js/me.js')
    .addEntry('health', './assets/js/health.js')
    .addEntry('medicalHistory', './assets/js/medicalHistory.js')
    .addEntry('buddy', './assets/js/buddy.js')

    .createSharedEntry('app', [
        './assets/js/app.js'
    ])

    .addEntry('desktop', './assets/js/desktop.js')
    .addEntry('doctor', './assets/js/doctor.js')
    .addEntry('patientSearch', './assets/js/patientSearch.js')

    // uncomment if you use Sass/SCSS files
    .enableSassLoader(function(sassOptions) {}, {
        resolveUrlLoader: false
    })


    // uncomment for legacy applications that require $/jQuery as a global variable
    .autoProvidejQuery()

    .enableSourceMaps(!Encore.isProduction())

    .cleanupOutputBeforeBuild()
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning(Encore.isProduction())

    // uncomment to define the assets of the project

    // .addEntry('app', './assets/js/app.js')
    // .addStyleEntry('css/app', './assets/css/app.scss')

    // uncomment if you use Sass/SCSS files
    // .enableSassLoader()

    // uncomment for legacy applications that require $/jQuery as a global variable
    // see http://symfony.com/doc/current/frontend/encore/bootstrap.html
    .autoProvidejQuery()
    .enableReactPreset()
;

var config = Encore.getWebpackConfig();
config.node =
    { fs: 'empty', net: 'empty', tls: 'empty', dns: 'empty' };

module.exports = config;
