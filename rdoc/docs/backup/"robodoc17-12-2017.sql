# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.19)
# Database: robodoc
# Generation Time: 2017-12-17 23:00:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chatbot_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `patient_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `patient_global_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token_last_refreshed_at` datetime DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `latitude` decimal(10,0) DEFAULT NULL,
  `longitude` decimal(10,0) DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;

INSERT INTO `account` (`id`, `chatbot_id`, `first_name`, `last_name`, `access_token`, `patient_id`, `patient_global_code`, `token_last_refreshed_at`, `last_login_at`, `latitude`, `longitude`, `city`, `country`)
VALUES
	(1,NULL,'Nathan','O\'Hanlon','xdOwQE0YSzMlV0ZMbZnH7zdv2baG','AAAA-0124-8','IFAUCQJNGAYTENBNHBAE6USJJ5HA','2017-12-17 22:57:28','2017-12-17 22:23:40',51,0,'London','United Kingdom'),
	(15,NULL,'Jeremy','Martin','9BaTDbJK3MfvRUvvjQIi16jDzlHZ','AAAA-0124-8','IFAUCQJNGAYTENBNHBAE6USJJ5HA',NULL,'2017-12-17 22:23:40',51,0,'London','United Kingdom'),
	(16,NULL,'James','Dean','9BaTDbJK3MfvRUvvjQIi16jDzlHZ','AAAA-0124-8','IFAUCQJNGAYTENBNHBAE6USJJ5HA',NULL,'2017-12-17 22:23:40',51,0,'Los Angeles','United States'),
	(17,NULL,'Peter','Smith','9BaTDbJK3MfvRUvvjQIi16jDzlHZ','AAAA-0124-8','IFAUCQJNGAYTENBNHBAE6USJJ5HA',NULL,'2017-12-17 22:23:40',51,0,'London','United Kingdom'),
	(18,NULL,'Simon','Stalone','9BaTDbJK3MfvRUvvjQIi16jDzlHZ','AAAA-0124-8','IFAUCQJNGAYTENBNHBAE6USJJ5HA',NULL,'2017-12-17 22:23:40',51,0,'Manchester','United Kingdom'),
	(19,NULL,'David','Peters','9BaTDbJK3MfvRUvvjQIi16jDzlHZ','AAAA-0124-8','IFAUCQJNGAYTENBNHBAE6USJJ5HA',NULL,'2017-12-17 22:23:40',51,0,'Leeds','United Kingdom'),
	(20,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(21,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(22,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(23,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(24,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(25,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(26,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(27,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(28,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(29,NULL,'Nathan','O\'Hanlon','J3AB7A6DLAUGc4k4mMbhhLBtthaA',NULL,NULL,'2017-12-17 22:54:42',NULL,NULL,NULL,NULL,NULL),
	(30,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(31,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(32,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(33,NULL,'Nathan','O\'Hanlon','ttWWSPzuEjGCVi8gGfYxfywDUrqB',NULL,NULL,'2017-12-17 22:55:16',NULL,NULL,NULL,NULL,NULL),
	(34,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(35,NULL,'Nathan','O\'Hanlon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table consultation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `consultation`;

CREATE TABLE `consultation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `symptom` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_fluid_filled` tinyint(1) DEFAULT NULL,
  `is_papular` tinyint(1) DEFAULT NULL,
  `colour` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_scaly` tinyint(1) DEFAULT NULL,
  `is_breakage` tinyint(1) DEFAULT NULL,
  `image_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pending` tinyint(1) DEFAULT NULL,
  `rash_black_or_blue` decimal(10,0) DEFAULT NULL,
  `rash_red_non_scaly` decimal(10,0) DEFAULT NULL,
  `rash_red_scaly` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_964685A69B6B5FBA` (`account_id`),
  CONSTRAINT `FK_964685A69B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `consultation` WRITE;
/*!40000 ALTER TABLE `consultation` DISABLE KEYS */;

INSERT INTO `consultation` (`id`, `account_id`, `symptom`, `is_fluid_filled`, `is_papular`, `colour`, `is_scaly`, `is_breakage`, `image_id`, `is_pending`, `rash_black_or_blue`, `rash_red_non_scaly`, `rash_red_scaly`)
VALUES
	(29,1,'rash',0,0,'red',1,0,'1513459730',1,NULL,NULL,NULL),
	(30,15,'rash',0,0,'red',1,0,'1513370346',1,NULL,NULL,NULL),
	(31,16,'rash',0,0,'red',1,0,'1513459314',1,NULL,NULL,NULL);

/*!40000 ALTER TABLE `consultation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table health_observation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `health_observation`;

CREATE TABLE `health_observation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `stress_raise` int(11) DEFAULT NULL,
  `hours_sleep` int(11) DEFAULT NULL,
  `fruit_or_veg_eaten` int(11) DEFAULT NULL,
  `water_drunk` int(11) DEFAULT NULL,
  `effective_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C705789E9B6B5FBA` (`account_id`),
  CONSTRAINT `FK_C705789E9B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `health_observation` WRITE;
/*!40000 ALTER TABLE `health_observation` DISABLE KEYS */;

INSERT INTO `health_observation` (`id`, `account_id`, `stress_raise`, `hours_sleep`, `fruit_or_veg_eaten`, `water_drunk`, `effective_date_time`)
VALUES
	(37,1,0,7,5,3,'2017-12-17 22:23:40'),
	(38,1,0,8,5,3,'2017-12-17 22:23:40'),
	(39,1,1,6,4,2,'2017-12-17 22:23:40'),
	(40,1,1,7,3,4,'2017-12-17 22:23:40'),
	(41,1,-1,8,6,3,'2017-12-17 22:23:40'),
	(42,1,1,8,4,4,'2017-12-17 22:23:40'),
	(43,1,0,8,5,1,'2017-12-17 22:23:40');

/*!40000 ALTER TABLE `health_observation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;

INSERT INTO `migration_versions` (`version`)
VALUES
	('20171211164939'),
	('20171211173645'),
	('20171211175253'),
	('20171211200543'),
	('20171211203316'),
	('20171212084932'),
	('20171212085832'),
	('20171212094134'),
	('20171212094456'),
	('20171213145016'),
	('20171213194021'),
	('20171214153541'),
	('20171215112536'),
	('20171215190033'),
	('20171215190718'),
	('20171215203432');

/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table observation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observation`;

CREATE TABLE `observation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `text` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value_quantity` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `effectiveDateTime` datetime DEFAULT NULL,
  `value_unit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C576DBE09B6B5FBA` (`account_id`),
  CONSTRAINT `FK_C576DBE09B6B5FBA` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `observation` WRITE;
/*!40000 ALTER TABLE `observation` DISABLE KEYS */;

INSERT INTO `observation` (`id`, `account_id`, `text`, `code`, `value_quantity`, `effectiveDateTime`, `value_unit`)
VALUES
	(226,1,'Intake of fruit and vegetables less than 5 portions daily','301991000000101',NULL,'2017-12-13 23:56:01',NULL),
	(227,1,'General CVD 10Y risk (Framingham)','65853-4','22','2017-11-22 06:19:29','%'),
	(228,1,'General CVD 10Y risk (Framingham)','65853-4','22','2017-10-20 09:57:18','%'),
	(229,1,'Smoker','72166-2','4','2017-10-20 09:57:15',NULL),
	(230,1,'Diastolic BP','8462-4','79','2017-10-20 09:57:14','mm/Hg'),
	(231,1,'Systolic BP','8480-6','123','2017-10-20 09:57:12','mm/Hg'),
	(232,1,'HDL','2085-9','79','2017-10-20 09:57:11','mg/dl'),
	(233,1,'LDL','18262-6','126','2017-10-20 09:57:10','mg/dl'),
	(234,1,'Smoker','72166-2','4','2017-10-17 10:45:44',NULL),
	(235,1,'Diastolic BP','8462-4','74','2017-10-17 10:45:43','mm/Hg'),
	(236,1,'Systolic BP','8480-6','100','2017-10-17 10:45:41','mm/Hg'),
	(237,1,'HDL','2085-9','42','2017-10-17 10:45:40','mg/dl'),
	(238,1,'LDL','18262-6','131','2017-10-17 10:45:39','mg/dl'),
	(239,1,'General CVD 10Y risk (Framingham)','65853-4','47','2017-10-16 13:27:42','%'),
	(240,1,'Smoker','72166-2','1','2017-10-16 18:26:32',NULL),
	(241,1,'General CVD 10Y risk (Framingham)','65853-4','33','2017-10-16 13:22:43','%'),
	(242,1,'Smoker','72166-2','4','2017-10-16 13:22:40',NULL),
	(243,1,'Diastolic BP','8462-4','97','2017-10-16 13:22:39','mm/Hg'),
	(244,1,'Systolic BP','8480-6','102','2017-10-16 13:22:38','mm/Hg'),
	(245,1,'HDL','2085-9','56','2017-10-16 13:22:36','mg/dl'),
	(246,1,'LDL','18262-6','139','2017-10-16 13:22:35','mg/dl'),
	(247,1,'Smoker','72166-2','1','2017-10-11 12:31:18',NULL),
	(248,1,'Smoker','72166-2','4','2017-10-09 17:31:27',NULL),
	(249,1,'Diastolic BP','8462-4','98','2017-10-09 17:31:26','mm/Hg'),
	(250,1,'Systolic BP','8480-6','138','2017-10-09 17:31:24','mm/Hg');

/*!40000 ALTER TABLE `observation` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
