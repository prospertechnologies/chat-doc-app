<?php


namespace App\Entity;

use App\Entity\Account;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RunningObservationRepository")
 */
class RunningObservation
{
    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer $stepCount
     * @ORM\Column(type="integer", name="step_count", nullable=true)
     */
    private $stepCount;

    /**
     * @var integer $metersRun
     * @ORM\Column(type="integer", name="meters_run", nullable=true)
     */
    private $metersRun;

    /**
     * @var Account $account
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="healthObservations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $account;

    /**
     * @var \DateTime $effectiveDateTime
     * @ORM\Column(type="datetime", length=100, name="effective_date_time", nullable=true)
     */
    private $effectiveDateTime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDateTime()
    {
        return $this->effectiveDateTime;
    }

    /**
     * @param \DateTime $effectiveDateTime
     */
    public function setEffectiveDateTime($effectiveDateTime)
    {
        $this->effectiveDateTime = $effectiveDateTime;
    }

    /**
     * @return int
     */
    public function getStepCount()
    {
        return $this->stepCount;
    }

    /**
     * @param int $stepCount
     */
    public function setStepCount($stepCount)
    {
        $this->stepCount = $stepCount;
    }

    /**
     * @return int
     */
    public function getMetersRun()
    {
        return $this->metersRun;
    }

    /**
     * @param int $metersRun
     */
    public function setMetersRun($metersRun)
    {
        $this->metersRun = $metersRun;
    }
}