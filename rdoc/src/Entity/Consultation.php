<?php


namespace App\Entity;


use App\Entity\Account;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ConsultationRepository")
 */
class Consultation
{
    const COLOUR_RED = 'red',
        COLOUR_BLACK = 'black';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $isPapular
     * @ORM\Column(type="string", length=20, name="symptom", nullable=true)
     */
    private $symptom;  //rash

    /**
     * @var boolean $isFluidFilled
     * @ORM\Column(type="boolean", name="is_fluid_filled", nullable=true)
     */
    private $isFluidFilled;

    /**
     * @var boolean $isPapular
     * @ORM\Column(type="boolean", name="is_papular", nullable=true)
     */
    private $isPapular;

    /**
     * @var string $colour
     * @ORM\Column(type="string", length=15, name="colour", nullable=true)
     */
    private $colour; //red

    /**
     * @var boolean $isScaly
     * @ORM\Column(type="boolean", name="is_scaly", nullable=true)
     */
    private $isScaly;

    /**
     * @var boolean $isPapular
     * @ORM\Column(type="boolean", name="is_breakage", nullable=true)
     */
    private $isBreakage;

    /**
     * @var string $imageId
     * @ORM\Column(type="string", length=10, name="image_id", nullable=true)
     */
    private $imageId;

    /**
     * @var boolean $isPending
     * @ORM\Column(type="boolean", name="is_pending", nullable=true)
     */
    private $isPending = true;

    /**
     * @var Account $account
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="consultations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $account;


    /**
     * Watson observation
     * @var decimal $rashBlackOrBlue
     * @ORM\Column(type="decimal", name="rash_black_or_blue", nullable=true)
     */
    private $rashBlackOrBlue;

    /**
     * Watson observation
     * @var decimal $rashRedNonScaly
     * @ORM\Column(type="decimal", name="rash_red_non_scaly", nullable=true)
     */
    private $rashRedNonScaly;

    /**
     * Watson observation
     * @var decimal $rashRedScaly
     * @ORM\Column(type="decimal", name="rash_red_scaly", nullable=true)
     */
    private $rashRedScaly;

    /**
     * Date first noticed
     * @var \DateTime $firstNoticedAt
     * @ORM\Column(type="datetime", name="first_noticed_at", nullable=true)
     */
    private $firstNoticedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSymptom()
    {
        return $this->symptom;
    }

    /**
     * @param mixed $symptom
     */
    public function setSymptom($symptom)
    {
        $this->symptom = $symptom;
    }

    /**
     * @return bool
     */
    public function isIsFluidFilled()
    {
        return $this->isFluidFilled;
    }

    /**
     * @param bool $isFluidFilled
     */
    public function setIsFluidFilled($isFluidFilled)
    {
        $this->isFluidFilled = $isFluidFilled;
    }

    /**
     * @return bool
     */
    public function isIsPapular()
    {
        return $this->isPapular;
    }

    /**
     * @param bool $isPapular
     */
    public function setIsPapular($isPapular)
    {
        $this->isPapular = $isPapular;
    }

    /**
     * @return string
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * @param string $colour
     */
    public function setColour($colour)
    {
        $this->colour = $colour;
    }

    /**
     * @return bool
     */
    public function isIsScaly()
    {
        return $this->isScaly;
    }

    /**
     * @param bool $isScaly
     */
    public function setIsScaly($isScaly)
    {
        $this->isScaly = $isScaly;
    }

    /**
     * @return bool
     */
    public function isIsBreakage()
    {
        return $this->isBreakage;
    }

    /**
     * @param bool $isBreakage
     */
    public function setIsBreakage($isBreakage)
    {
        $this->isBreakage = $isBreakage;
    }

    /**
     * @return string
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param string $imageId
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }


    /**
     * @return mixed
     */
    public function getRashBlackOrBlue()
    {
        return $this->rashBlackOrBlue;
    }

    /**
     * @param mixed $rashBlackOrBlue
     */
    public function setRashBlackOrBlue($rashBlackOrBlue)
    {
        $this->rashBlackOrBlue = $rashBlackOrBlue;
    }

    /**
     * @return mixed
     */
    public function getRashRedNonScaly()
    {
        return $this->rashRedNonScaly;
    }

    /**
     * @param mixed $rashRedNonScaly
     */
    public function setRashRedNonScaly($rashRedNonScaly)
    {
        $this->rashRedNonScaly = $rashRedNonScaly;
    }

    /**
     * @return decimal
     */
    public function getRashRedScaly()
    {
        return $this->rashRedScaly;
    }

    /**
     * @param decimal $rashRedScaly
     */
    public function setRashRedScaly($rashRedScaly)
    {
        $this->rashRedScaly = $rashRedScaly;
    }

    /**
     * @return bool
     */
    public function isIsPending()
    {
        return $this->isPending;
    }

    /**
     * @param bool $isPending
     */
    public function setIsPending($isPending)
    {
        $this->isPending = $isPending;
    }

    /**
     * @return \DateTime
     */
    public function getFirstNoticedAt()
    {
        return $this->firstNoticedAt;
    }

    /**
     * @param \DateTime $firstNoticedAt
     */
    public function setFirstNoticedAt($firstNoticedAt)
    {
        $this->firstNoticedAt = $firstNoticedAt;
    }

}