<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ObservationRepository")
 */
class Observation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="observations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $account;

    /**
     * @ORM\Column(type="string", length=100, name="text", nullable=true)
     */
    private $text;


    /**
     * @ORM\Column(type="string", length=100, name="code", nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=100, name="value_quantity", nullable=true)
     */
    private $valueQuantity;

    /**
     * @ORM\Column(type="string", length=100, name="value_unit", nullable=true)
     */
    private $valueUnit;

    /**
     * @ORM\Column(type="datetime", length=100, name="effectiveDateTime", nullable=true)
     */
    private $effectiveDateTime;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account = null)
    {
        $this->account = $account;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getValueQuantity()
    {
        return $this->valueQuantity;
    }

    /**
     * @param mixed $valueQuantity
     */
    public function setValueQuantity($valueQuantity)
    {
        $this->valueQuantity = $valueQuantity;
    }

    /**
     * @return mixed
     */
    public function getEffectiveDateTime()
    {
        return $this->effectiveDateTime;
    }

    /**
     * @param mixed $effectiveDateTime
     */
    public function setEffectiveDateTime($effectiveDateTime)
    {
        $this->effectiveDateTime = $effectiveDateTime;
    }

    /**
     * @return mixed
     */
    public function getValueUnit()
    {
        return $this->valueUnit;
    }

    /**
     * @param mixed $valueUnit
     */
    public function setValueUnit($valueUnit)
    {
        $this->valueUnit = $valueUnit;
    }

}
