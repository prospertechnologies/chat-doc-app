<?php


namespace App\Entity;

use App\Entity\Account;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HealthObservationRepository")
 */
class HealthObservation
{


    /**
     * @var integer $id
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer $stressRaise
     * @ORM\Column(type="integer", name="stress_raise", nullable=true)
     */
    private $stressRaise;

    /**
     * @var integer $hoursSleep
     * @ORM\Column(type="integer", name="hours_sleep", nullable=true)
     */
    private $hoursSleep;

    /**
     * @var integer $fruitOrVegEaten
     * @ORM\Column(type="integer", name="fruit_or_veg_eaten", nullable=true)
     */
    private $fruitOrVegEaten;

    /**
     * @var integer $waterDrunk
     * @ORM\Column(type="integer", name="water_drunk", nullable=true)
     */
    private $waterDrunk;

    /**
     * @var Account $account
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="healthObservations")
     * @ORM\JoinColumn(nullable=true)
     */
    private $account;

    /**
     * @var \DateTime $effectiveDateTime
     * @ORM\Column(type="datetime", length=100, name="effective_date_time", nullable=true)
     */
    private $effectiveDateTime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStressRaise()
    {
        return $this->stressRaise;
    }

    /**
     * @param int $stressRaise
     */
    public function setStressRaise($stressRaise)
    {
        $this->stressRaise = $stressRaise;
    }

    /**
     * @return int
     */
    public function getHoursSleep()
    {
        return $this->hoursSleep;
    }

    /**
     * @param int $hoursSleep
     */
    public function setHoursSleep($hoursSleep)
    {
        $this->hoursSleep = $hoursSleep;
    }

    /**
     * @return mixed
     */
    public function getFruitOrVegEaten()
    {
        return $this->fruitOrVegEaten;
    }

    /**
     * @param mixed $fruitOrVegEaten
     */
    public function setFruitOrVegEaten($fruitOrVegEaten)
    {
        $this->fruitOrVegEaten = $fruitOrVegEaten;
    }

    /**
     * @return int
     */
    public function getWaterDrunk()
    {
        return $this->waterDrunk;
    }

    /**
     * @param int $waterDrunk
     */
    public function setWaterDrunk($waterDrunk)
    {
        $this->waterDrunk = $waterDrunk;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDateTime()
    {
        return $this->effectiveDateTime;
    }

    /**
     * @param \DateTime $effectiveDateTime
     */
    public function setEffectiveDateTime($effectiveDateTime)
    {
        $this->effectiveDateTime = $effectiveDateTime;
    }
}