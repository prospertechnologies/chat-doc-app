<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    public function __construct()
    {
        $this->observations = new ArrayCollection();
        $this->healthObservations = new ArrayCollection();
        $this->consultations = new ArrayCollection();
        $this->runningObservations = new ArrayCollection();

    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $chatbotId;

    /**
     * @ORM\Column(type="string", name="patient_id", length=100, nullable=true)
     */
    private $patientId;

    /**
     * @ORM\Column(type="string", name="patient_global_code", length=100, nullable=true)
     */
    private $patientGlobalCode;

    /**
     * @ORM\Column(type="string", length=100, name="first_name", nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100, name="last_name", nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=100, name="access_token", nullable=true)
     */
    private $accessToken;

    /**
     * @var \DateTime $tokenLastRefreshedAt
     * @ORM\Column(type="datetime", length=100, name="token_last_refreshed_at", nullable=true)
     */
    private $tokenLastRefreshedAt;

    /**
     * An observation is one that comes from the Orion system.
     * @ORM\OneToMany(targetEntity="App\Entity\Observation", mappedBy="account", orphanRemoval=true)
     */
    private $observations;

    /**
     * A health observation is one made by the app itself, before adding it to Orion.
     * @ORM\OneToMany(targetEntity="App\Entity\HealthObservation", mappedBy="account", orphanRemoval=true)
     */
    private $healthObservations;

    /**
     * A consultation able to be observed by a doctor
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation", mappedBy="account", orphanRemoval=true)
     */
    private $consultations;

    /**
     * @var \DateTime $lastLoginAt
     * @ORM\Column(type="datetime", name="last_login_at", nullable=true)
     */
    private $lastLoginAt;

    /**
     * @ORM\Column(type="decimal", name="latitude", precision=10, scale=6, nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", name="longitude", precision=10, scale=6, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=100, name="city", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=100, name="country", nullable=true)
     */
    private $country;

    //+44 7373 37373

    /**
     * @ORM\Column(type="string", length=30, name="mobile", nullable=true)
     */
    private $mobile;


    private $runningObservations;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getChatbotId()
    {
        return $this->chatbotId;
    }

    /**
     * @param mixed $chatbotId
     */
    public function setChatbotId($chatbotId)
    {
        $this->chatbotId = $chatbotId;
    }

    /**
     * @return mixed
     */
    public function getPatientId()
    {
        return $this->patientId;
    }

    /**
     * @param mixed $patientId
     */
    public function setPatientId($patientId)
    {
        $this->patientId = $patientId;
    }

    /**
     * @return mixed
     */
    public function getPatientGlobalCode()
    {
        return $this->patientGlobalCode;
    }

    /**
     * @param mixed $patientGlobalCode
     */
    public function setPatientGlobalCode($patientGlobalCode)
    {
        $this->patientGlobalCode = $patientGlobalCode;
    }

    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getTokenLastRefreshedAt()
    {
        return $this->tokenLastRefreshedAt;
    }

    /**
     * @param mixed $tokenLastRefreshedAt
     */
    public function setTokenLastRefreshedAt($tokenLastRefreshedAt)
    {
        $this->tokenLastRefreshedAt = $tokenLastRefreshedAt;
    }

    public function getTokenIsExpired(\DateTime $currentDateTime)
    {
        if (null == $this->getTokenLastRefreshedAt()) {
            return true;
        } else {
            $tokenLastRefreshedAt = $this->getTokenLastRefreshedAt();
        }

        $interval = $currentDateTime->getTimestamp() - $tokenLastRefreshedAt->getTimestamp();

        $hours = 3;

        $isExpired = ($interval > ($hours * 60 * 60));

        return $isExpired;

    }

    /**
     * @return Collection|Observation
     */
    public function getObservations()
    {
        return $this->observations;
    }

    public function addObservation(Observation $observation)
    {
        if ($this->observations->contains($observation)) {
            return;
        }

        $this->observations[] = $observation;
        $observation->setAccount($this);
    }

    public function removeObservation(Observation $observation)
    {
        $this->observations->removeElement($observation);
        $observation->setAccount(null);
    }

    /**
     * @return mixed
     */
    public function getHealthObservations()
    {
        return $this->healthObservations;
    }

    public function addHealthObservation(HealthObservation $healthObservation)
    {
        if ($this->healthObservations->contains($healthObservation)) {
            return;
        }

        $this->healthObservations[] = $healthObservation;
        $healthObservation->setAccount($this);
    }

    public function removeHealthObservation(HealthObservation $healthObservation)
    {
        $this->healthObservations->removeElement($healthObservation);
        $healthObservation->setAccount(null);
    }

    /**
     * @return mixed
     */
    public function getConsultations()
    {
        return $this->consultations;
    }

    public function addConslutation(Consultation $consultation)
    {
        if ($this->consultations->contains($consultation)) {
            return;
        }

        $this->consultations[] = $consultation;
        $consultation->setAccount($this);
    }

    public function removeConsultation(Consultation $consultation)
    {
        $this->consultations->removeElement($consultation);
        $consultation->setAccount(null);
    }


    /**
     * @return \DateTime
     */
    public function getLastLoginAt()
    {
        return $this->lastLoginAt;
    }

    /**
     * @param \DateTime $lastLoginAt
     */
    public function setLastLoginAt($lastLoginAt)
    {
        $this->lastLoginAt = $lastLoginAt;
    }

    /**
     * @return decimal
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param decimal $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return decimal
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param decimal $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }


    public function addRunningObservation(RunningObservation $runningObservation)
    {
        if ($this->runningObservations == null) {
            $this->runningObservations = new ArrayCollection();
        }
        if ($this->runningObservations->contains($runningObservation)) {
            return;
        }

        $this->runningObservations[] = $runningObservation;
        $runningObservation->setAccount($this);
    }

    public function setRunningObservations(array $runningObservationsArray)
    {
        foreach ($runningObservationsArray as $runningObservation) {
            $this->addRunningObservation($runningObservation);
        }
    }

    public function getRunningObservations()
    {
        return $this->runningObservations;
    }

    public function getTotalMetersRun()
    {
        $total = 0;
        /**
         * @var RunningObservation $runningObservation
         */
        foreach ($this->getRunningObservations() as $runningObservation) {
            $total += $runningObservation->getMetersRun();
        }

        return $total;
    }


    public function getMonthsMetersRun($month, $year)
    {
        $total = 0;
        /**
         * @var RunningObservation $runningObservation
         */
        foreach ($this->getRunningObservationsForMonth($month, $year) as $runningObservation) {

            $total += $runningObservation->getMetersRun();
        }

        return $total;
    }


    public function getTotalStepCount()
    {
        $total = 0;
        /**
         * @var RunningObservation $runningObservation
         */
        foreach ($this->getRunningObservations() as $runningObservation) {
            $total += $runningObservation->getStepCount();
        }

        return $total;
    }
}
