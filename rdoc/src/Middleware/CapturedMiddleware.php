<?php


namespace App\Middleware;


use App\Entity\Consultation;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Interfaces\Middleware\Captured;
use BotMan\BotMan\Interfaces\Middleware\Received;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use Doctrine\ORM\EntityManagerInterface;

class CapturedMiddleware implements Captured
{
    private $em;
    /**
     * @var Consultation $consultation
     */
    private $consultation;


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
    }

    private function persistAndSaveConslutation() {
        $this->em->persist($this->consultation);
        $this->em->flush();
    }

    /**
     * Handle an incoming message.
     *
     * @param IncomingMessage $message
     * @param callable $next
     * @param BotMan $bot
     *
     * @return mixed
     */
    public function captured(IncomingMessage $message, $next, BotMan $bot)
    {
        $text = $this->getText($message);

        if (in_array($text, ['disease', 'infection', 'cut', 'rash',])) {
            $this->consultation->setSymptom($text);
        } elseif ($text == 'no_fluid_filled_lesions') {
            $this->consultation->setIsFluidFilled(false);
        } elseif ($text == 'no_not_papular') {
            $this->consultation->setIsPapular(false);
        } elseif ($text == 'yes_is_red') {
            $this->consultation->setColour('red');
        } elseif ($text == 'yes_is_scaly') {
            $this->consultation->setIsScaly(true);
        } elseif ($text == 'no_not_scaly') {
            $this->consultation->setIsScaly(false);
        } elseif ($text == 'yes_is_broken') {
            $this->consultation->setIsBreakage(true);
        } elseif ($text == 'no_not_broken') {
            $this->consultation->setIsBreakage(false);
        }

        $this->persistAndSaveConslutation();

        return $message;
    }


    private function getText(IncomingMessage $message)
    {
        $text = $message->getText();
        $text = substr($text, 0, strpos($text, '-'));

        return $text;
    }
}