<?php


namespace App\Conversations;

use App\Entity\Account;
use App\Entity\Consultation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use Doctrine\ORM\EntityManagerInterface;

class MedicalConversation extends Conversation
{

    private $consultation;
    private $em;


    public function __construct(EntityManagerInterface $em, Account $account, Consultation $consultation)
    {
        $this->em = $em;
        $this->consultation = $consultation;
    }

    private function getText(Answer $answer)
    {
        $text = $answer->getText();
        $text = substr($text, 0, strpos($text, '-'));

        return $text;
    }

    /**
     * First question
     */
    public function askReason()
    {
        $user = $this->bot->getUser();

        $question = Question::create("Hi, I'm RoboDOC, what can I help you with today?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Can you take a look at my symptoms')->value('symptoms'),
                Button::create("I'm not feeling well")->value('ill'),
                Button::create("I have an emergency & need help ASAP")->value('emergency'),
            ]);

        return $this->ask($question, function (Answer $answer) {

            $user = $this->bot->getUser();

            $text = $this->getText($answer);
            if ($text === 'symptoms') {
                $this->symptomType();
            } elseif ($text === 'ill') {
                $this->say('Please lie down first, then call a doctor.');
            } elseif ($text === 'emergency') {
                $this->say('Please call 999 imediately.');
            } else {
                $this->say("Sorry I couldn't understand you. Please try 'hi' or a button.");
            }
        });
    }

    public function symptomType()
    {
        $diseaseOrInfectionQuestion = Question::create('What kind of symptom is it?')
            ->fallback("I didn't understand your response")
            ->callbackId('symptom_type')
            ->addButtons([
                Button::create('A Disease')->value('disease'),
                Button::create('An Infection')->value('infection'),
                Button::create('A cut or bruise')->value('cut'),
                Button::create('A rash')->value('rash')
            ]);


        return $this->ask($diseaseOrInfectionQuestion, function (Answer $answer) {
            $text = $this->getText($answer);
            if ($text === 'infection') {
                $this->consultation->setSymptom('infection');
                $this->infectionType();
            } elseif ($text == 'rash') {
                $this->consultation->setSymptom('rash');
                $this->rashType1();
            } else {
                //disease
                return $this->say('We are sending anti-biotics, please hold on.');
            }
        });
    }

    public function infectionType()
    {
        $infectionTypeQuestion = Question::create('What type of infection is it?')
            ->fallback("I didn't understand your response")
            ->callbackId('infection_type')
            ->addButtons([
                Button::create('Skin infection')->value('skin_infection'),
                Button::create('Internal infection')->value('internal_infection')
            ]);

        return $this->ask($infectionTypeQuestion, function (Answer $answer) {
            $text = $this->getText($answer);
            if ($text === 'skin_infection') {
                $this->consultation->setSymptom('Skin infection');
                return $this->askForImages('Please upload an image of it.', function ($images) {
                    // $images contains an array with all images.
                });
            } else {
                $this->persistAndSaveConslutation();
                return $this->say('Please take some panadol.');
            }
        });
    }


    public function rashType1()
    {
        $rashTypeQuestion1 = Question::create('Does it contain any fluid? (fluid filled lesions)')
            ->fallback("I didn't understand your response")
            ->callbackId('rash_type_lesions')
            ->addButtons([
                Button::create('Yes')->value('yes_fluid_filled_lesions'),
                Button::create('No')->value('no_fluid_filled_lesions')
            ]);

        return $this->ask($rashTypeQuestion1, function (Answer $answer) {
            $text = $this->getText($answer);
            if ($text === 'no_fluid_filled_lesions') {
                $this->consultation->setIsFluidFilled(false);
                $this->rashType2NoFluidFilledLesions();
            } else {
                $this->consultation->setIsFluidFilled(true);
                $this->persistAndSaveConslutation();
                return $this->say('Please consult your doctor immediately.');
            }
        });
    }

    public function rashType2NoFluidFilledLesions()
    {
        $rashTypeQuestion2 = Question::create('Is it papular (raised and bumpy)?')
            ->fallback("I didn't understand your response")
            ->callbackId('rash_type_papular')
            ->addButtons([
                Button::create('Yes')->value('yes_is_papular'),
                Button::create('No')->value('no_not_papular')
            ]);

        return $this->ask($rashTypeQuestion2, function (Answer $answer) {
            $text = $this->getText($answer);
            if ($text === 'no_not_papular') {
                $this->consultation->setIsPapular(false);
                $this->rashType3NotPapular();
            } else {
                $this->consultation->setIsPapular(true);
                $this->persistAndSaveConslutation();
                return $this->say('Please consult your doctor immediately.');
            }
        });
    }

    private function persistAndSaveConslutation() {
        $this->em->persist($this->consultation);
        $this->em->flush();
    }

    public function rashType3NotPapular()
    {
        $rashTypeQuestion3 = Question::create('Is it red?')
            ->fallback("I didn't understand your response")
            ->callbackId('rash_type_red')
            ->addButtons([
                Button::create('Yes')->value('yes_is_red'),
                Button::create('No')->value('no_not_red')
            ]);

        return $this->ask($rashTypeQuestion3, function (Answer $answer) {
            $text = $this->getText($answer);
            if ($text === 'yes_is_red') {
                $this->consultation->setColour(Consultation::COLOUR_RED);
                $this->rashType4Red();
            } else {
                $this->persistAndSaveConslutation();
                return $this->say('Please consult your doctor immediately.');
            }
        });
    }

    public function rashType4Red()
    {
        $rashTypeQuestion4 = Question::create('Is it scaly?')
            ->fallback("I didn't understand your response")
            ->callbackId('rash_type_scaly')
            ->addButtons([
                Button::create('Yes')->value('yes_is_scaly'),
                Button::create('No')->value('no_not_scaly')
            ]);

        return $this->ask($rashTypeQuestion4, function (Answer $answer) {
            $text = $this->getText($answer);
            if ($text === 'yes_is_scaly') {
                $this->consultation->setIsScaly(true);
                $this->rashType5EpidermalBreakage();
            } else {
                $this->consultation->setIsScaly(false);
                $this->persistAndSaveConslutation();
                return $this->say('Please consult your doctor immediately.');
            }
        });
    }

    public function rashType5EpidermalBreakage()
    {
        $rashTypeQuestion4 = Question::create('Does it have any breakage?')
            ->fallback("I didn't understand your response")
            ->callbackId('rash_type_breakage')
            ->addButtons([
                Button::create('Yes')->value('yes_is_broken'),
                Button::create('No')->value('no_not_broken')
            ]);

        return $this->ask($rashTypeQuestion4, function (Answer $answer) {
            $text = $this->getText($answer);
            if ($text === 'no_not_broken') {
                $this->consultation->setIsBreakage(false);
                $this->persistAndSaveConslutation();
                return $this->askForImages('Please upload an image of it.', function ($images) {
                    // $images contains an array with all images.
                });
            } else {
                $this->consultation->setIsBreakage(true);
                $this->persistAndSaveConslutation();
                return $this->say('Please consult your doctor immediately.');
            }
        });
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askReason();
    }
}