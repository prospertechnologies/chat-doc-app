<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConsultationController extends BaseController
{
    /**
     * @Route("/consultation", name="consultation")
     */
    public function index()
    {
        return $this->render('Consultation/index.html.twig', array_merge($this->getGlobalVars(), ['activeConsultation' => 'class=active']));
    }

}