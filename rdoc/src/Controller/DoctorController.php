<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Consultation;
use App\Entity\RunningObservation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DoctorController extends BaseController
{
    /**
     * @Route("/doctor/account", name="doctor_account")
     */
    public function account(EntityManagerInterface $em)
    {
        return $this->render('Doctor/account.html.twig', [
            'pageName' => 'doctorAccount'
        ]);
    }


    /**
     * @Route("/doctor/resolve-triage", name="doctor_resolve_triage", methods={"POST"})
     */
    public function resolveTriage(Request $request, EntityManagerInterface $em)
    {
        $params = urldecode($request->getContent());
        $keyValues = [];

        foreach (explode('&', $params) as $chunk) {
            $param = explode("=", $chunk);

            if ($param) {
                $keyValues[urldecode($param[0])] = urldecode($param[1]);
            }
        }

        $consultationId = (int)$keyValues['consultation_id'];

        $consultationRepo = $em->getRepository(Consultation::class);
        /**
         * @var Consultation $consultation
         */
        $consultation = $consultationRepo->find($consultationId);

        if (null == $consultation) {
            return new JsonResponse(array('success' => false, 'message' => 'Consultation was not found'));
        }

        $consultation->setIsPending(false);
        $em->persist($consultation);
        $em->flush();

        return new JsonResponse(array('success' => true));
    }

    /**
     * @Route("/doctor/deploy-drone", name="doctor_deploy_drone", methods={"POST"})
     */
    public function deployDrone(Request $request, EntityManagerInterface $em, \Swift_Mailer $mailer)
    {
        $params = urldecode($request->getContent());
        $keyValues = [];

        foreach (explode('&', $params) as $chunk) {
            $param = explode("=", $chunk);

            if ($param) {
                $keyValues[urldecode($param[0])] = urldecode($param[1]);
            }
        }

        $accountNum = (int)$keyValues['account_id'];
        if ($accountNum > 0) {
            $accountRepo = $em->getRepository(Account::class);
            $account = $accountRepo->find($accountNum);
        } else {
            $account = $this->getAccount($em);
        }

        //make the CSV for the warehouse.


        $fileFullPath = $this->makeCsvForDroneDelivery($account);

        $attachment = \Swift_Attachment::fromPath($fileFullPath);
        $attachment->setFilename('Chat_Bot_Subscription_' . $keyValues['consultation_id'] . '.csv');


        try {

            //using app password for chatdoc: https://myaccount.google.com/u/1/apppasswords
            $message = (new \Swift_Message('Drone Delivery for Perscription '.$keyValues['consultation_id']))
                ->setFrom('nathanlon@gmail.com')
                ->setTo($this->getParameter('warehouse_email'))
                ->setBody(
                    $this->renderView(
                        'Doctor/emails/droneDelivery.html.twig',
                        array_merge(array(
                            'account' => $account,
                        ), $keyValues)
                    ),
                    'text/html'
                )
                ->attach($attachment)
            ;

            $mailer->send($message);
        } catch(\Exception $e) {
            return new JsonResponse(array('success' => false, 'message' => $e->getMessage()));
        }

        return new JsonResponse(array('success' => false));
    }


    private function makeCsvForDroneDelivery(Account $account, $warehouseLat = null, $warehouseLng = null)
    {
        if ((null == $warehouseLat) || (null == $warehouseLng)) {
            $warehouseLat = $this->getParameter('warehouse_latitude');
            $warehouseLng = $this->getParameter('warehouse_longitude');
        }

        $csvOutput = $this->renderView('Doctor/emails/csvForDroneDeploy.csv.twig', array(
            'warehouseLat' => $warehouseLat,
            'warehouseLng' => $warehouseLng,
            'account' => $account
        ));
        $fileName = $account->getId() . '-' . time() . '.csv';

        $fileFullPath = dirname(__FILE__) . '/../../assets/csv/'. $fileName;
        file_put_contents($fileFullPath, $csvOutput);

        return $fileFullPath;
    }

        /**
     * @Route("/doctor/login", name="doctor_login")
     */
    public function logout(EntityManagerInterface $em, \Swift_Mailer $mailer)
    {

        return $this->render('Doctor/login.html.twig', [
            'pageName' => 'doctorLogin'
        ]);
    }

    /**
     * @Route("/doctor", name="doctor")
     */
    public function index(EntityManagerInterface $em)
    {
        $consultationRepo = $em->getRepository(Consultation::class);
        $consultations =  $consultationRepo->findAllPending();

        return $this->render('Doctor/index.html.twig', [
            'pageName' => 'doctor',
            'consultations' => $consultations
        ]);
    }


    /**
     * @Route("/doctor/patient-search", name="doctor_patient_search")
     */
    public function patientSearch(EntityManagerInterface $em, Request $request)
    {
        $accountRepo = $em->getRepository(Account::class);
        //$accounts =  $accountRepo->findAll();

        $q = $request->get('q');

        $accountRepo = $em->getRepository(Account::class);
        $accounts = $accountRepo->searchByLastName($q);

        $runningObRepo = $em->getRepository(RunningObservation::class);

        $accountsWithHealth = array();
        foreach ($accounts as $account) {

            $runningObs = $runningObRepo->findBy(array('account' => $account));
            $account->setRunningObservations($runningObs);

            $accountsWithHealth[] = $account;
        }

        return $this->render('Doctor/patientSearch.html.twig', [
            'pageName' => 'patientSearch',
            'patients' => $accountsWithHealth,
            'q' => $q
        ]);
    }


    /**
     * @Route("/doctor/trends", name="doctor_trends")
     */
    public function trends(EntityManagerInterface $em)
    {

        return $this->render('Doctor/trends.html.twig', [
            'pageName' => 'trends'
        ]);
    }
}