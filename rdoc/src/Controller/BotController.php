<?php

namespace App\Controller;

use App\Botman\Drivers\Web\WebDriver;
use App\Entity\Consultation;
use App\Middleware\CapturedMiddleware;
use App\Middleware\HeardMiddleware;
use App\Middleware\ReceivedMiddleware;
use App\Service\Chatbot;
use App\Service\Watson;
use BotMan\BotMan\Messages\Attachments\Image;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Conversations\MedicalConversation;

class BotController extends BaseController
{
    private $account;

    private $em;
    /**
     * @Route("/bot", name="bot")
     */
    public function bot(Request $request, Chatbot $chatbot, EntityManagerInterface $em,
                        CapturedMiddleware $capturedMiddleware)
    {
        $botman = $chatbot->getBotman();
        $driver = $chatbot->getDriver();

        $driver->buildPayload($request);

        $account = $this->getAccount($em);

        //find a new or existing consultation.
        $consultationRepo = $em->getRepository(Consultation::class);

        $pendingConsultations = $consultationRepo->findPendingForAccount($account);

        if (count($pendingConsultations) == 0) {
            $pendingConsultation = new Consultation();
            $pendingConsultation->setIsPending(true);
            $pendingConsultation->setAccount($account);
        } else {
            $pendingConsultation = $pendingConsultations[0];
        }

        $capturedMiddleware->setConsultation($pendingConsultation);

        $botman->middleware->heard(new HeardMiddleware());
        $botman->middleware->received(new ReceivedMiddleware());
        $botman->middleware->captured($capturedMiddleware);

        //$botman->say('Hi there', 9999999);

//        $botman->hears('symptoms', function ($bot) {
//            $bot->reply('Is it a disease or infection?');
//        });

//        $botman->hears('bar', function ($bot) {
//            $bot->reply('Hello Bar');
//        });

        $account = $this->getAccount($em);
        $this->account = $account;
        $this->em = $em;

        $botman->hears('hi', function ($bot) {

            $consultation = new Consultation();
            $consultation->setAccount($this->account);

            $medicalConversation = new MedicalConversation($this->em, $this->account, $consultation);
            $bot->startConversation($medicalConversation);
        });

        $botman->listen();

        return $driver->messageHandled();
    }


        /**
     * @Route("/upload", name="bot_upload")
     */
    public function upload(Request $request, Chatbot $chatbot, Watson $watson, EntityManagerInterface $em)
    {
        /**
         * @var UploadedFile $uploadedFile
         */
        $uploadedFile = $request->files->get('upload');
        $userId = $request->get('user');

        $account = $this->getAccount($em);

        $fileName = time() . '.jpg';

        $botman = $chatbot->getBotman();
        $driver = $chatbot->getDriver();

        $botman->listen();

        $pathToMoveTo = dirname(__FILE__) . '/../../public/uploads';

        $uploadedFile->move($pathToMoveTo, $fileName);

        $url = 'https://robodoc.co/uploads/' . $fileName;

        //Training see: https://watson-visual-recognition.ng.bluemix.net/
        $results = $watson->recogniseImage($fileName);

        $repo = $em->getRepository(Consultation::class);

        /**
         * @var Consultation $pendingConsultation
         */
        $pendingConsultations = $repo->findPendingForAccount($account);

        if (count($pendingConsultations) == 0) {
            $pendingConsultation = new Consultation();
            $pendingConsultation->setAccount($account);
            $pendingConsultation->setIsPending(true);
        } else {
            $pendingConsultation = $pendingConsultations[0];
        }

        $features = '';
        foreach ($results as $result) {
            $features .= $result['class'] . '('.$result['score'].'), ';

            if ($result['class'] == 'RashBlackOrBlue') {
                $pendingConsultation->setRashBlackOrBlue((float)$result['score']);
            } elseif ($result['class'] == 'RashRedNonScaley') {
                $pendingConsultation->setRashRedNonScaly((float)$result['score']);
            } elseif ($result['class'] == 'RashRedScaley') {
                $pendingConsultation->setRashRedScaly((float)$result['score']);
            }
        }

        $em->persist($pendingConsultation);
        $em->flush();

        $watsonObservation = $this->getWatsonObservation($pendingConsultation);

        //and we are processing it now. The recognised features were:'. $features

        $botman->say('Thanks we have received your image. A doctor will look at the results ASAP. We have analysed the image with AI.'.$watsonObservation, $userId, $driver, array(
            'url' => $url
        ));

        return $driver->messageHandled();
    }

    /**
     * Compare the results from the saved consultation, with what Watson finds.
     * If image is black, and patient said it was red, may be colour blind - for example.
     *
     * @param Consultation $consultation
     * @return string
     */
    private function getWatsonObservation(Consultation $consultation)
    {
        $watsonObservation = '';
        if ($consultation->getColour() == $consultation::COLOUR_RED) {
            if ($consultation->getRashBlackOrBlue() > 0.7) {
                $watsonObservation .= ' The colour seems to be black or blue, but you said it was red.';
            }
        }

        if ($consultation->isIsScaly()) {
            if ($consultation->getRashRedNonScaly() > 0.7) {
                $watsonObservation .= ' The rash does not seem to be scaly, but you said it was scaly.';
            }
        }

        if ($watsonObservation == '') {
            $watsonObservation = ' The answers you gave seem to match the image! Well done. A doctor should be in contact soon.';
        } else {
            $watsonObservation .= ' Chat Doc will note this to the doctor triaging this.';
        }

        return $watsonObservation;
    }

    /**
     * @Route("/upload-receive", name="bot_upload-receive")
     */
    public function uploadReceive(Request $request, Chatbot $chatbot)
    {
        $botman = $chatbot->getBotman();
        $driver = $chatbot->getDriver();

        $botman->receivesImages(function($botman, $images) {

            foreach ($images as $image) {

                $url = $image->getUrl(); // The direct url
                $title = $image->getTitle(); // The title, if available
                $payload = $image->getPayload(); // The original payload
            }
        });

        $botman->listen();

        return $driver->messageHandled();
    }



    /**
     * @Route("/pre-upload", name="bot_pre_upload")
     */
    public function preUpload(Request $request)
    {
        return $this->renderView('me/preUpload.html.twig');
    }
}