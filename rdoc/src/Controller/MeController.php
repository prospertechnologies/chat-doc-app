<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\HealthObservation;
use App\Entity\Observation;
use App\Repository\HealthObservationRepository;
use App\Repository\ObservationRepository;
use App\Service\Orion;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MeController extends BaseController
{

    /**
     * @Route("/", name="me")
     */
    public function index(EntityManagerInterface $em)
    {
        $account = $this->getAccount($em);

        return $this->render('Me/index.html.twig', [
            'account' => $account,
            'pageName' => 'me'
        ] + array_merge($this->getGlobalVars(), ['activeMe' => 'class=active']));
    }

    /**
     * @Route("/me/medical-history", name="me_medical_history")
     */
    public function history(EntityManagerInterface $em, Request $request)
    {
        $account = $this->getAccount($em);

        //get observations in db already.
        $observations = $account->getObservations();

        /**
         * @var ObservationRepository $observationRepo
         */
        $observationRepo = $em->getRepository(Observation::class);

        $bloodPressureDiastolic = $this->getMonthsData($observationRepo, $account, ObservationRepository::TYPE_BLOOD_PRESSURE_DIASTOLIC);
        $bloodPressureSystolic = $this->getMonthsData($observationRepo, $account, ObservationRepository::TYPE_BLOOD_PRESSURE_SYSTOLIC);
        $cholesterolLdl = $this->getMonthsData($observationRepo, $account, ObservationRepository::TYPE_CHOLESTEROL_LDL);
        $cholesterolHdl = $this->getMonthsData($observationRepo, $account, ObservationRepository::TYPE_CHOLESTEROL_HDL);

        $healthObservationRepo = $em->getRepository(HealthObservation::class);

        $today = new \DateTime();
        $currentMonth = (int)$today->format('m');
        $currentYear = (int)$today->format('Y');

        $lastMonthNum = $currentMonth - 1;
        if ($lastMonthNum == 0) {
            $lastMonthNum = 12;
            $currentYear--;
        }
        $lastMonth = $today->setDate($currentYear, $lastMonthNum, 1);

        list($healthStress, $healthSleep, $healthFruitAndVeg, $healthWater) =
            $this->getHealthDataForMonth($healthObservationRepo, $account, $lastMonthNum, $currentYear);

        return $this->render('Me/medicalHistory.html.twig', [
            'observationsBloodPressureDiastolic' => $bloodPressureDiastolic,
            'observationsBloodPressureSystolic' => $bloodPressureSystolic,
            'observationsCholesterolLdl' => $cholesterolLdl,
            'observationsCholesterolHdl' => $cholesterolHdl,
            'healthStress' => $healthStress,
            'healthSleep' => $healthSleep,
            'healthFruitAndVeg' => $healthFruitAndVeg,
            'healthWater' => $healthWater,
            'error' => $request->get('error', false),
            'month' => $lastMonth->format('M')
        ] + $this->getGlobalVars());

    }



    /**
     * @Route("/me/observations/login", name="me_observe_login")
     */
    public function observeLogin(Request $request, EntityManagerInterface $em)
    {
        $account = $this->getAccount($em);

        //temp to test - set expired always.
//        $date = new \DateTime('now -4 days');
//        $account->setTokenLastRefreshedAt($date);

        $tokenExpired = $account->getTokenIsExpired(new \DateTime());
        if ($tokenExpired) {
            $host = $request->getHost();

            $url = 'https://auth.orionhealth.io/oauth2/authorize?client_id=' . $this->getParameter('orion_client_id') . '&redirect_uri=' .
                'https://' . $host . $this->generateUrl('me_observe_redirect') . '&response_type=token';

            return $this->redirect($url);
        } else {
            return $this->redirect($this->generateUrl('me_observations'));
        }
    }


    /**
     * @Route("/me/observations", name="me_observe_redirect")
     */
    public function observeRedirect(Request $request, EntityManagerInterface $em)
    {
        if ($request->getMethod() == Request::METHOD_POST) {

            $token = $request->request->get('token');

            $account = $this->getAccount($em);

            if (null != $account) {
                $account->setAccessToken($token);
                $account->setTokenLastRefreshedAt(new \DateTime());
                $em->persist($account);
                $em->flush();

                return $this->redirect($this->generateUrl('me_observations'));
            }
        }

        return $this->render('Me/observeRedirect.html.twig', $this->getGlobalVars());



        $token = '';
        $hasToken = false;
        if ($request->cookies->has('access_token')) {
            $hasToken = true;
            $token = $request->cookies->get('access_token');

            $account = $this->getAccount($em);

            if (null != $account) {

                $existingToken = $account->getAccessToken();
                if ($existingToken == $token) {
                    $isExpired = $account->getTokenIsExpired(new \DateTime());
                    if ($isExpired) {
                        $hasToken = false;
                    } else {
                        $account->setAccessToken($token);
                        $account->setTokenLastRefreshedAt(new \DateTime());
                        $em->persist($account);
                        $em->flush();
                    }
                } else {
                    $account->setAccessToken($token);
                    $account->setTokenLastRefreshedAt(new \DateTime());
                    $em->persist($account);
                    $em->flush();
                }
            }
        }

        if ($hasToken == false) {
            return $this->render('Me/observeRedirect.html.twig', $this->getGlobalVars() + ['hasToken' => $hasToken, 'token' => $token]);
        } else {
            return $this->redirect($this->generateUrl('me_observations'));
        }
    }

    /**
     * @Route("/me/observations-old", name="me_observe_redirect_old")
     */
    public function observeRedirectOLD(Request $request, EntityManagerInterface $em)
    {
        $token = '';
        $hasToken = false;
        if ($request->cookies->has('access_token')) {
            $hasToken = true;
            $token = $request->cookies->get('access_token');

            $account = $this->getAccount($em);

            if (null != $account) {

                $existingToken = $account->getAccessToken();
                if ($existingToken == $token) {
                    $isExpired = $account->getTokenIsExpired(new \DateTime());
                    if ($isExpired) {
                        $hasToken = false;
                    } else {
                        $account->setAccessToken($token);
                        $account->setTokenLastRefreshedAt(new \DateTime());
                        $em->persist($account);
                        $em->flush();
                    }
                } else {
                    $account->setAccessToken($token);
                    $account->setTokenLastRefreshedAt(new \DateTime());
                    $em->persist($account);
                    $em->flush();
                }
            }
        }

        if ($hasToken == false) {
            return $this->render('Me/observeRedirect.html.twig', $this->getGlobalVars() + ['hasToken' => $hasToken, 'token' => $token]);
        } else {
            return $this->redirect($this->generateUrl('me_observations'));
        }
    }

    /**
     * @Route("/me/observations2", name="me_observations")
     */
    public function observations(Request $request, EntityManagerInterface $em, Orion $orion)
    {

        $repo = $em->getRepository(Account::class);
        $account = $repo->find(1);

        if (null == $account) {
            $account = new Account();
            $account->setFirstName('Nathan');
            $account->setLastName("O'Hanlon");

            $em->persist($account);
            $em->flush();
        }

        $observationsRepo = $em->getRepository(Observation::class);
        $existingObservations = $observationsRepo->findBy(array('account' => $account));

        foreach ($existingObservations as $existingObservation) {
            $em->remove($existingObservation);
        }
        $em->flush();

        //make observation.

        try {
            $observations = $orion->retrieveObservations($account);
        } catch(\Exception $e) {
            return $this->redirect($this->generateUrl('me_medical_history', ['error' => 1]));
        }

        if (null == $observations) {
            return $this->redirect($this->generateUrl('me_medical_history', ['error' => 2]));
        }

        foreach ($observations->entry as $entry) {

            $o = new Observation();
            $text = $entry->resource->code->text;
            $o->setText($text);
            $code = $entry->resource->code->coding[0]->code;
            $o->setCode($code);
            $valueQuantity = null;
            $valueUnit = null;
            if (isset($entry->resource->valueQuantity)) {
                $valueQuantity = $entry->resource->valueQuantity->value;
                $valueUnit = $entry->resource->valueQuantity->unit;
            } elseif (isset($entry->resource->valueString)) {
                $valueQuantity = $entry->resource->valueString;
            }
            $o->setValueQuantity($valueQuantity);
            $o->setValueUnit($valueUnit);
            //format: 2017-10-09T17:31:27-04:00
            $effectiveDateTime = $entry->resource->effectiveDateTime;
            $date = \DateTime::createFromFormat(\DateTime::RFC3339, $effectiveDateTime);
            $o->setEffectiveDateTime($date);

            $o->setAccount($account);
            $em->persist($o);
        }

        $em->flush();

        //return new JsonResponse($observations);

        return $this->redirect($this->generateUrl('me_medical_history'));

        // replace this line with your own code!
        //return $this->render('@Maker/demoPage.html.twig', [ 'path' => str_replace($this->getParameter('kernel.project_dir').'/', '', __FILE__) ]);
    }

    /**
     * @param $observationRepo
     * @param $account
     * @param $type (eg: ObservationRepository::TYPE_BLOOD_PRESSURE_DIASTOLIC
     * @return array
     */
    private function getMonthsData($observationRepo, $account, $type)
    {
        $dataPoints = $observationRepo->findTypeForAccount($account, $type);

        $rawDate = [];
        foreach ($dataPoints as $dataPoint) {
            $dayOfMonth = $dataPoint->getEffectiveDateTime()->format('j');
            $rawDate[$dayOfMonth] = $dataPoint;
        }


        $dayOfMonth = 8; //usually 1
        $monthData = [];
        while ($dayOfMonth < 22) { //usually 32

            $monthData[$dayOfMonth] = null;
            if (isset($rawDate[$dayOfMonth])) {
                $monthData[$dayOfMonth] = $rawDate[$dayOfMonth];
            }

            $dayOfMonth++;
        }
        return $monthData;
    }


    private function getHealthDataForMonth(HealthObservationRepository $healthObservationRepo, Account $account, $monthNum, $yearNum)
    {
        $accountsHealthObservations = $healthObservationRepo->findByAccountForMonth($account, $monthNum, $yearNum);

        $stressData = $sleepData = $fruitAndVegData = $waterData = [];

        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $monthNum, $yearNum);

        $count = 1;
        while ($count <= $daysInMonth) {
            $stressData[$count] = null;
            $sleepData[$count] = null;
            $fruitAndVegData[$count] = null;
            $waterData[$count] = null;
            $count++;
        }

        $count = 1;
        /**
         * @var HealthObservation $healthObservation
         */
        foreach ($accountsHealthObservations as $healthObservation) {

            $dayOfMonth = (int)$healthObservation->getEffectiveDateTime()->format('j');

            $stressData[$dayOfMonth] = $healthObservation->getStressRaise();
            $sleepData[$dayOfMonth] = $healthObservation->getHoursSleep();
            $fruitAndVegData[$dayOfMonth] = $healthObservation->getFruitOrVegEaten();
            $waterData[$dayOfMonth] = $healthObservation->getWaterDrunk();

            $count++;
        }

        return array(
            $stressData,
            $sleepData,
            $fruitAndVegData,
            $waterData
        );
//        $monthData = [
//            HealthObservationRepository::TYPE_STRESS =>
//        ];
//
//        foreach ($accountsHealthObservations as $healthObservation) {
//            if ($type == HealthObservationRepository::TYPE_STRESS) {
//                $monthData[]
//            }
//        }
    }

}
