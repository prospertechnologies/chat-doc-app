<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\HealthObservation;
use App\Entity\RunningObservation;
use App\Service\Orion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HealthController extends BaseController
{
    /**
     * @Route("/health", name="health")
     */
    public function index(EntityManagerInterface $em)
    {
        $account = $this->getAccount($em);

        return $this->render('Health/index.html.twig', array_merge($this->getGlobalVars(), [
            'activeHealth' => 'class=active',
            'accountId' => $account->getId()
        ]));
    }

    /**
     * @Route("/health/observation", name="health_observation")
     */
    public function observation(Request $request, EntityManagerInterface $em, Orion $orionService)
    {
        $stressRaise =      (int)$request->get('stress_raise');
        $hoursSleep =       (int)$request->get('hours_sleep');
        $fruitOrVegEaten =  (int)$request->get('fruit_or_veg_eaten');
        $waterDrunk =       (int)$request->get('water_drunk');

        $healthObservation = new HealthObservation();
        $healthObservation->setStressRaise($stressRaise);
        $healthObservation->setHoursSleep($hoursSleep);
        $healthObservation->setFruitOrVegEaten($fruitOrVegEaten);
        $healthObservation->setWaterDrunk($waterDrunk);
        $healthObservation->setAccount($this->getAccount($em));

        $em->persist($healthObservation);
        $em->flush();

        try {
            $orionService->sendObservations($healthObservation);

        } catch(\Exception $e) {
            return JsonResponse::create(array('success' => false, 'message' => 'There was a problem, please contact support.'));
        }

        return JsonResponse::create(array('success' => true));
    }

    /**
     * @Route("/running/observation", name="running_observation")
     */
    public function runningObservation(Request $request, EntityManagerInterface $em, Orion $orionService)
    {
        $params = urldecode($request->getContent());
        $keyValues = [];

        foreach (explode('&', $params) as $chunk) {
            $param = explode("=", $chunk);

            if ($param) {
                $keyValues[urldecode($param[0])] = urldecode($param[1]);
            }
        }



        $accountId = $keyValues['accountId'];
        $stepCount = $keyValues['stepCount'];
        $metersRun = $keyValues['metersRun'];

        $accountRepo = $em->getRepository(Account::class);
        $account = $accountRepo->find($accountId);

        $runningObservation = new RunningObservation();
        $runningObservation->setAccount($account);
        $runningObservation->setEffectiveDateTime(new \DateTime());
        $runningObservation->setStepCount($stepCount);
        $runningObservation->setMetersRun($metersRun);
        $em->persist($runningObservation);
        $em->flush();

        try {
            $orionService->sendRunningObservations($runningObservation);

        } catch(\Exception $e) {
            return JsonResponse::create(array('success' => false, 'message' => 'There was a problem, please contact support.'));
        }


        return new JsonResponse(array('success' => true));
    }

}