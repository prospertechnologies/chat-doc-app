<?php


namespace App\Controller;


use App\Entity\Account;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /**
     * @param EntityManagerInterface $em
     * @return Account|null|object
     */
    protected function getAccount(EntityManagerInterface $em)
    {
        $repo = $em->getRepository(Account::class);
        $account = $repo->find(1);

        if (null == $account) {
            $account = new Account();
            $account->setFirstName('Nathan');
            $account->setLastName("O'Hanlon");
            $account->setId(1);

            $em->persist($account);
            $em->flush();
            return $account;
        }
        return $account;
    }

    protected function getGlobalVars()
    {
        return array(
            'pageName' => '',
            'activeMe' => '',
            'activeHealth' => '',
            'activeConsultation' => ''
        );

    }


}