<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BuddyController extends BaseController
{
    /**
     * @Route("/buddy", name="buddy")
     */
    public function index()
    {
        return $this->render('Buddy/index.html.twig', $this->getGlobalVars());
    }

}