<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171215190033 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consultation ADD symptom TINYINT(1) DEFAULT NULL, ADD is_fluid_filled TINYINT(1) DEFAULT NULL, ADD is_papular TINYINT(1) DEFAULT NULL, ADD colour VARCHAR(15) DEFAULT NULL, ADD is_scaly TINYINT(1) DEFAULT NULL, ADD is_breakage TINYINT(1) DEFAULT NULL, ADD image_id VARCHAR(10) DEFAULT NULL, ADD is_pending TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consultation DROP symptom, DROP is_fluid_filled, DROP is_papular, DROP colour, DROP is_scaly, DROP is_breakage, DROP image_id, DROP is_pending');
    }
}
