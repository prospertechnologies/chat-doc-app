<?php


namespace App\DataFixtures;

use App\Entity\HealthObservation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class HealthObservationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $today = new \DateTime();
        $month = (int)$today->format('m');
        $year = (int)$today->format('Y');

        $month--;
        if ($month == 0) {
            $month = 12;
            $year--;
        }

        $daysLastMonth = $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $count = 1;
        while ($count <= $daysLastMonth) {
            $date = new \DateTime();
            $ho = new HealthObservation();
            $ho->setAccount($this->getReference('a1'));
            $ho->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ho->setFruitOrVegEaten(5);
            $ho->setHoursSleep(7);
            $ho->setStressRaise(0);
            $ho->setWaterDrunk(3);
            $manager->persist($ho);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ho = new HealthObservation();
            $ho->setAccount($this->getReference('a1'));
            $ho->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ho->setFruitOrVegEaten(5);
            $ho->setHoursSleep(8);
            $ho->setStressRaise(0);
            $ho->setWaterDrunk(3);
            $manager->persist($ho);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ho = new HealthObservation();
            $ho->setAccount($this->getReference('a1'));
            $ho->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ho->setFruitOrVegEaten(4);
            $ho->setHoursSleep(6);
            $ho->setStressRaise(1);
            $ho->setWaterDrunk(2);
            $manager->persist($ho);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ho = new HealthObservation();
            $ho->setAccount($this->getReference('a1'));
            $ho->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ho->setFruitOrVegEaten(3);
            $ho->setHoursSleep(7);
            $ho->setStressRaise(1);
            $ho->setWaterDrunk(4);
            $manager->persist($ho);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ho = new HealthObservation();
            $ho->setAccount($this->getReference('a1'));
            $ho->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ho->setFruitOrVegEaten(6);
            $ho->setHoursSleep(8);
            $ho->setStressRaise(-1);
            $ho->setWaterDrunk(3);
            $manager->persist($ho);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ho = new HealthObservation();
            $ho->setAccount($this->getReference('a1'));
            $ho->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ho->setFruitOrVegEaten(4);
            $ho->setHoursSleep(8);
            $ho->setStressRaise(1);
            $ho->setWaterDrunk(4);
            $manager->persist($ho);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ho = new HealthObservation();
            $ho->setAccount($this->getReference('a1'));
            $ho->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ho->setFruitOrVegEaten(5);
            $ho->setHoursSleep(8);
            $ho->setStressRaise(0);
            $ho->setWaterDrunk(1);
            $manager->persist($ho);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            AccountFixtures::class,
        );
    }
}