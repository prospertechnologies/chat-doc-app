<?php


namespace App\DataFixtures;

use App\Entity\RunningObservation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class RunningObservationFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $today = new \DateTime();
        $month = (int)$today->format('m');
        $year = (int)$today->format('Y');

        $month--;
        if ($month == 0) {
            $month = 12;
            $year--;
        }

        $daysLastMonth = $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $count = 1;
        while ($count <= $daysLastMonth) {
            $date = new \DateTime();
            $ro = new RunningObservation();
            $ro->setAccount($this->getReference('a1'));
            $ro->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ro->setMetersRun(rand(1000, 3000));
            $ro->setStepCount($ro->getMetersRun() * 0.8);
            $manager->persist($ro);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ro = new RunningObservation();
            $ro->setAccount($this->getReference('a1'));
            $ro->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ro->setMetersRun(rand(1000, 3000));
            $ro->setStepCount($ro->getMetersRun() * 0.8);
            $manager->persist($ro);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ro = new RunningObservation();
            $ro->setAccount($this->getReference('a1'));
            $ro->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ro->setMetersRun(rand(1000, 3000));
            $ro->setStepCount($ro->getMetersRun() * 0.8);
            $manager->persist($ro);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ro = new RunningObservation();
            $ro->setAccount($this->getReference('a1'));
            $ro->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ro->setMetersRun(rand(1000, 3000));
            $ro->setStepCount($ro->getMetersRun() * 0.8);
            $manager->persist($ro);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ro = new RunningObservation();
            $ro->setAccount($this->getReference('a1'));
            $ro->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ro->setMetersRun(rand(1000, 3000));
            $ro->setStepCount($ro->getMetersRun() * 0.8);
            $manager->persist($ro);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ro = new RunningObservation();
            $ro->setAccount($this->getReference('a1'));
            $ro->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ro->setMetersRun(rand(1000, 3000));
            $ro->setStepCount($ro->getMetersRun() * 0.8);
            $manager->persist($ro);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
            $date = new \DateTime();
            $ro = new RunningObservation();
            $ro->setAccount($this->getReference('a1'));
            $ro->setEffectiveDateTime($date->setDate($year, $month, $count));
            $ro->setMetersRun(rand(1000, 3000));
            $ro->setStepCount($ro->getMetersRun() * 0.8);
            $manager->persist($ro);

            $count++;
            if ($count > $daysLastMonth) {
                break;
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            AccountFixtures::class,
        );
    }
}