<?php

namespace App\DataFixtures;

use App\Entity\Account;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AccountFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $connection = $manager->getConnection();
        $connection->exec("ALTER TABLE account AUTO_INCREMENT = 1;");

        $account = new Account();
        $account->setFirstName('Nathan');
        $account->setLastName("O'Hanlon");
        $account->setLastLoginAt(new \DateTime());
        $account->setCity('London');
        $account->setCountry('United Kingdom');
        $account->setLatitude(51.462843);
        $account->setLongitude(-0.205830);
        $account->setPatientId('AAAA-0124-8');
        $account->setAccessToken('9BaTDbJK3MfvRUvvjQIi16jDzlHZ');
        $account->setPatientGlobalCode('IFAUCQJNGAYTENBNHBAE6USJJ5HA');
        $account->setMobile('+44 7373 37373');
        $manager->persist($account);
        $this->addReference('a1', $account);


        $account = new Account();
        $account->setFirstName('Jeremy');
        $account->setLastName("Martin");
        $account->setLastLoginAt(new \DateTime('-2 days'));
        $account->setCity('London');
        $account->setCountry('United Kingdom');
        $account->setLatitude(51.462843);
        $account->setLongitude(-0.205830);
        $account->setPatientId('AAAA-0124-8');
        $account->setAccessToken('9BaTDbJK3MfvRUvvjQIi16jDzlHZ');
        $account->setPatientGlobalCode('IFAUCQJNGAYTENBNHBAE6USJJ5HA');
        $account->setMobile('+44 7373 37373');
        $manager->persist($account);
        $this->addReference('a2', $account);

        $account = new Account();
        $account->setFirstName('James');
        $account->setLastName("Dean");
        $account->setLastLoginAt(new \DateTime('-4 days -2 hours -24 minutes'));
        $account->setCity('Los Angeles');
        $account->setCountry('United States');
        $account->setLatitude(51.462843);
        $account->setLongitude(-0.205830);
        $account->setPatientId('AAAA-0124-8');
        $account->setAccessToken('9BaTDbJK3MfvRUvvjQIi16jDzlHZ');
        $account->setPatientGlobalCode('IFAUCQJNGAYTENBNHBAE6USJJ5HA');
        $account->setMobile('+44 7373 37373');
        $manager->persist($account);
        $this->addReference('a3', $account);

        $account = new Account();
        $account->setFirstName('Peter');
        $account->setLastName("Smith");
        $account->setLastLoginAt(new \DateTime('-8 days -2 hours'));
        $account->setCity('London');
        $account->setCountry('United Kingdom');
        $account->setLatitude(51.462843);
        $account->setLongitude(-0.205830);
        $account->setPatientId('AAAA-0124-8');
        $account->setAccessToken('9BaTDbJK3MfvRUvvjQIi16jDzlHZ');
        $account->setPatientGlobalCode('IFAUCQJNGAYTENBNHBAE6USJJ5HA');
        $account->setMobile('+44 7373 37373');
        $manager->persist($account);
        $this->addReference('a4', $account);

        $account = new Account();
        $account->setFirstName('Simon');
        $account->setLastName("Stalone");
        $account->setLastLoginAt(new \DateTime());
        $account->setCity('Manchester');
        $account->setCountry('United Kingdom');
        $account->setLatitude(51.462843);
        $account->setLongitude(-0.205830);
        $account->setPatientId('AAAA-0124-8');
        $account->setAccessToken('9BaTDbJK3MfvRUvvjQIi16jDzlHZ');
        $account->setPatientGlobalCode('IFAUCQJNGAYTENBNHBAE6USJJ5HA');
        $account->setMobile('+44 7373 37373');
        $manager->persist($account);
        $this->addReference('a5', $account);

        $account = new Account();
        $account->setFirstName('David');
        $account->setLastName("Peters");
        $account->setLastLoginAt(new \DateTime());
        $account->setCity('Leeds');
        $account->setCountry('United Kingdom');
        $account->setLatitude(51.462843);
        $account->setLongitude(-0.205830);
        $account->setPatientId('AAAA-0124-8');
        $account->setAccessToken('9BaTDbJK3MfvRUvvjQIi16jDzlHZ');
        $account->setPatientGlobalCode('IFAUCQJNGAYTENBNHBAE6USJJ5HA');
        $account->setMobile('+44 7373 37373');
        $manager->persist($account);
        $this->addReference('a6', $account);

        $manager->flush();

    }
}