<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Consultation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ConsultationFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $c = new Consultation();
        $c->setAccount($this->getReference('a1'));
        $c->setColour('red');
        $c->setIsBreakage(false);
        $c->setIsFluidFilled(false);
        $c->setIsPapular(false);
        $c->setIsScaly(true);
        $c->setSymptom('rash');
        $c->setImageId(1513459864);
        $c->setIsPending(true);
        $c->setFirstNoticedAt(new \DateTime('2017-11-15T00:00:00'));
        $manager->persist($c);

        $c = new Consultation();
        $c->setAccount($this->getReference('a2'));
        $c->setColour('red');
        $c->setIsBreakage(false);
        $c->setIsFluidFilled(false);
        $c->setIsPapular(false);
        $c->setIsScaly(true);
        $c->setSymptom('rash');
        $c->setImageId(1513370346);
        $c->setIsPending(true);
        $c->setFirstNoticedAt(new \DateTime('2017-11-14T00:00:00'));
        $manager->persist($c);

        $c = new Consultation();
        $c->setAccount($this->getReference('a3'));
        $c->setColour('red');
        $c->setIsBreakage(false);
        $c->setIsFluidFilled(false);
        $c->setIsPapular(false);
        $c->setIsScaly(true);
        $c->setSymptom('rash');
        $c->setImageId(1513459314);
        $c->setIsPending(true);
        $c->setFirstNoticedAt(new \DateTime('2017-11-13T00:00:00'));
        $manager->persist($c);

        $manager->flush();

    }
}