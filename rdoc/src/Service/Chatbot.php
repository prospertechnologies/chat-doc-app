<?php


namespace App\Service;

use App\Botman\Drivers\Web\WebDriverSymfony;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\SymfonyCache;
use BotMan\BotMan\Drivers\DriverManager;
use Doctrine\Common\Cache\FilesystemCache;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

//use BotMan\Drivers\Web\WebDriver;
//use App\BotMan\Drivers\Web\WebDriver;

class Chatbot
{
    private $botman;

    private $driver;

    /**
     * @return BotMan
     */
    public function getBotman()
    {
        if (null != $this->botman) {
            return $this->botman;
        }

        $config = [

            /*
            |--------------------------------------------------------------------------
            | Web verification
            |--------------------------------------------------------------------------
            |
            | This array will be used to match incoming HTTP requests against your
            | web endpoint, to see if the request should match the web driver.
            |
            */
            'matchingData' => [
                'driver' => 'web',
            ],
        ];

        DriverManager::loadDriver(WebDriverSymfony::class);

        $cache = new FilesystemAdapter();

        // create an instance
        $this->botman = BotManFactory::create($config, new SymfonyCache($cache));

        $this->driver = $this->botman->getDriver();

        return $this->botman;

    }

    public function getDriver()
    {
        return $this->driver;
    }

}