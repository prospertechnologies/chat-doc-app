<?php

namespace App\Service;

use GuzzleHttp\Client;


class Watson
{
    private $key;

    public function __construct($key)
    {
        $this->key = $key;
    }

    public function recogniseImage($fileName)
    {
//        $fileName = 'fruitbowl.jpg';

        $client = new Client();
        $res = $client->request('POST', 'https://gateway-a.watsonplatform.net/visual-recognition/api/v3/classify?api_key='.$this->key . '&version=2016-05-20', [


            'multipart' => [
                [
                    'name' => 'parameters',
                    'contents' => '{ "classifier_ids": ["Rash_156085002"], "threshold": 0 }'
                ],
                [
                    'name'     => 'images_file',
                    'contents' => fopen(dirname(__FILE__) . '/../../public/uploads/'.$fileName, 'r'),//this path is image save temperary path
                ],
//
            ]
        ]);

        $body = \GuzzleHttp\json_decode($res->getBody(), true);

        $foundKeys = array();

        if (count($body['images'][0]['classifiers']) == 0) {
            return $foundKeys;
        }

        $classes = $body['images'][0]['classifiers'][0]['classes'];
        foreach ($classes as $class) {
            $foundKeys[] = $class;
        }

        return $foundKeys;
    }


}