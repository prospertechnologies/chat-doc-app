<?php

namespace App\Service;

use App\Entity\Account;
use App\Entity\HealthObservation;
use App\Entity\RunningObservation;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Orion
{
    const
        SNOMED_CODE_FRUIT_VEG_MORE_THAN_5 = 301961000000107,
        SNOMED_DESCRIPTION_FRUIT_VEG_MORE_THAN_5 = "Intake of fruit and vegetables at least 5 portions daily",
        SNOMED_CODE_FRUIT_VEG_LESS_THAN_5 = 301991000000101,
        SNOMED_DESCRIPTION_FRUIT_VEG_LESS_THAN_5 = "Intake of fruit and vegetables less than 5 portions daily",

        SNOMED_ORAL_FLUID_INTAKE = 302118008,
        SNOMED_ORAL_FLUID_INTAKE_DESCRIPTION = "Finding of measures of oral fluid intake (finding)",
        SNOMED_STRESS_INCREASED = 23085004,
        SNOMED_STRESS_INCREASED_DESCRIPTION = "Increased stress",
        SNOMED_STRESS_DECREASED = 79365001,
        SNOMED_STRESS_DECREASED_DESCRIPTION = "Decreased stress",

        SNOMED_CODE_EXCERCISE = 111222888,
        SNOMED_CODE_EXCERCISE_DESCRIPTION = "Excercise in meters run",

        SNOMED_CODE_STEP_COUNT = 222333888,
        SNOMED_CODE_STEP_COUNT_DESCRIPTION = "Step count";


    private $consumerKey;
    private $consumerSecret;
    private $endpointBase;

    public function __construct($consumerKey, $consumerSecret, $endpointBase, $systemNamespace)
    {
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->endpointBase = $endpointBase;
        $this->systemNamespace = $systemNamespace;
    }

    public function retrieveObservations(Account $account)
    {
        $endpoint = '/fhir/1.0/Observation/_search';
        $token = $account->getAccessToken();

        $subjectIdentifier = $this->systemNamespace . '%7c' . $account->getPatientId();

        $client = new Client();
        $res = $client->post($this->endpointBase . $endpoint . '?subject.identifier=' . $subjectIdentifier,
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$account->getAccessToken(),
                    'Accept' => 'application/json'
                ]
            ]);

        $body = $res->getBody();

        $allResponse = '';
        while (!$body->eof()) {
            $allResponse .= $body->read(1024);
        }

        return \GuzzleHttp\json_decode($allResponse);
    }

    public function sendObservations(HealthObservation $healthObservation)
    {

        $account = $healthObservation->getAccount();
        $client = new Client();

        if ($healthObservation->getStressRaise() == 1) {
            //SNOMED_STRESS_INCREASED
            $this->sendObservation($account,
                self::SNOMED_STRESS_INCREASED,
                self::SNOMED_STRESS_INCREASED_DESCRIPTION,
                $client);
        } elseif ($healthObservation->getStressRaise() == -1) {
            //SNOMED_STRESS_DECREASED
            $this->sendObservation($account,
                self::SNOMED_STRESS_DECREASED,
                self::SNOMED_STRESS_DECREASED_DESCRIPTION,
                $client);
        }

        if ($healthObservation->getFruitOrVegEaten() > 5) {
            //SNOMED_CODE_FRUIT_VEG_MORE_THAN_5
            $this->sendObservation($account,
                self::SNOMED_CODE_FRUIT_VEG_MORE_THAN_5,
                self::SNOMED_DESCRIPTION_FRUIT_VEG_MORE_THAN_5,
                $client);
        } else {
            //SNOMED_CODE_FRUIT_VEG_LESS_THAN_5
            $this->sendObservation($account,
                self::SNOMED_CODE_FRUIT_VEG_LESS_THAN_5,
                self::SNOMED_DESCRIPTION_FRUIT_VEG_LESS_THAN_5,
                $client);
        }

        if ($healthObservation->getWaterDrunk() > 0) {
            //SNOMED_ORAL_FLUID_INTAKE
            $this->sendObservation($account,
                self::SNOMED_ORAL_FLUID_INTAKE,
                self::SNOMED_ORAL_FLUID_INTAKE_DESCRIPTION,
                $client);
        }
    }

    public function sendRunningObservations(RunningObservation $runningObservation)
    {
        $client = new Client();
        $account = $runningObservation->getAccount();

        $excerciseSnomedCode = self::SNOMED_CODE_EXCERCISE;
        $excerciseSnomedDescription = self::SNOMED_CODE_EXCERCISE_DESCRIPTION;
        $excerciseValue = $runningObservation->getMetersRun();

        $this->sendObservation($account,
            $excerciseSnomedCode,
            $excerciseSnomedDescription,
            $client,
            $excerciseValue);

        $stepCountCode = self::SNOMED_CODE_STEP_COUNT;
        $stepCountDescription = self::SNOMED_CODE_STEP_COUNT_DESCRIPTION;
        $stepCountValue = $runningObservation->getStepCount();

        $this->sendObservation($account,
            $stepCountCode,
            $stepCountDescription,
            $client,
            $stepCountValue);


    }


    public function sendObservation(Account $account, $snomedCode, $snomedDescription, Client $client, $value = null)
    {
        $token = $account->getAccessToken();
        $patientGlobalCode = $account->getPatientGlobalCode();
        $patientId = $account->getPatientId();
        $endpoint = '/fhir/1.0/Observation/';

        //\DateTime::RFC3339
        $effectiveDateTime = new \DateTime();
        $effectiveDateTimeString = $effectiveDateTime->format(\DateTime::RFC3339);

        $valueString = '';
        if ($value != null) {
            $valueString = ',"valueString": "'. $value .'"';
        }

        $client = new Client();

        $body = <<<EOF
{
  "resourceType": "Observation",
  "status": "final",
  "code": {
    "coding": [
      {
        "system": "http://loinc.org",
        "code": "$snomedCode"
      }
    ],
    "text": "$snomedDescription"
  },
  "subject": {
    "reference": "Patient/$patientGlobalCode"
  },
  "effectiveDateTime": "$effectiveDateTimeString",
  "performer": [
    {
      "reference": "Practitioner/ROBODOC",
      "display": "RoboDoc"
    }
  ]
  $valueString
}
EOF;


        $res = $client->post($this->endpointBase . $endpoint,
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$account->getAccessToken(),
                    'Accept' => 'application/json'
                ],
                'body' => $body
            ]);

        $responseBody = $res->getBody();

        $allResponse = '';
        while (!$responseBody->eof()) {
            $allResponse .= $responseBody->read(1024);
        }

        return \GuzzleHttp\json_decode($allResponse);
    }


}