<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\Observation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ObservationRepository extends ServiceEntityRepository
{
    const
        TYPE_BLOOD_PRESSURE_DIASTOLIC = '8462-4',
        TYPE_BLOOD_PRESSURE_SYSTOLIC = '8480-6',
        TYPE_CHOLESTEROL_LDL = '18262-6', //Low Density Lipoprotein
        TYPE_CHOLESTEROL_HDL = '2085-9'; //High Density Lipoprotein


    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Observation::class);
    }

    public function findTypeForAccount(Account $account, $type)
    {
        return $this->createQueryBuilder('o')
            ->where('o.code = :code')->setParameter('code', $type)
            ->orderBy('o.effectiveDateTime', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
