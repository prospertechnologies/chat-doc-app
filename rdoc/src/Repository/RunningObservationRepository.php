<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\HealthObservation;
use App\Entity\RunningObservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RunningObservationRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RunningObservation::class);
    }

}
