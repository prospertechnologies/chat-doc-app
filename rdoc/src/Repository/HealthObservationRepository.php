<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\HealthObservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class HealthObservationRepository extends ServiceEntityRepository
{
    const
        TYPE_STRESS = 'stressRaise',
        TYPE_SLEEP = 'hoursSleep',
        TYPE_FRUIT_AND_VEG = 'fruitOrVegEaten',
        TYPE_WATER = 'waterDrunk';

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HealthObservation::class);
    }


    public function findByAccount(Account $account)
    {
        return $this->createQueryBuilder('ho')
            ->where('ho.account = :account')->setParameter('account', $account)
            ->getQuery()
            ->getResult()
            ;
    }


    public function findByAccountForMonth(Account $account, $monthNum, $yearNum)
    {
        $startDate = new \DateTime();
        $startDate->setDate($yearNum, $monthNum, 1);
        $startDate->setTime(0,0,0);

        $endDate = new \DateTime();
        if ($monthNum == 12) {
            $monthNum = 1;
            $yearNum++;
        }

        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $monthNum, $yearNum); // 31

        $endDate->setDate($yearNum, $monthNum, $daysInMonth);
        $endDate->setTime(23, 59, 59);

        return $this->createQueryBuilder('ho')
            ->where('ho.account = :account')->setParameter('account', $account)
            ->andWhere('ho.effectiveDateTime > :startDate')->setParameter('startDate', $startDate)
            ->andWhere('ho.effectiveDateTime < :endDate')->setParameter('endDate', $endDate)
            ->getQuery()
            ->getResult()
            ;
    }
}
