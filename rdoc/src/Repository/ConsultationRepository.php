<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\Consultation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ConsultationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Consultation::class);
    }

    public function findPendingForAccount(Account $account)
    {
        return $this->createQueryBuilder('c')
            ->where('c.account = :account')->setParameter('account', $account)
            ->andWhere('c.isPending = true')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAllPending()
    {
        return $this->createQueryBuilder('c')
            ->where('c.isPending = true')
            ->getQuery()
            ->getResult()
            ;
    }

}
