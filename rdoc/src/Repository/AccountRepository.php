<?php

namespace App\Repository;

use App\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function searchByLastName($lastName)
    {
        return $this->createQueryBuilder('a')
            ->where('a.lastName LIKE :lastName')->setParameter('lastName', $lastName)
            ->orderBy('a.lastName', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
